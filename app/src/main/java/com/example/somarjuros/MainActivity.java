package com.example.somarjuros;

import android.provider.DocumentsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView textValorFinal, textValorTaxa, textPercent;
    EditText editValorIni;
    SeekBar seekBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editValorIni = (EditText) findViewById(R.id.editValorIni);
        textValorFinal = findViewById(R.id.textValorFim);
        textValorTaxa = findViewById(R.id.textValorTaxa);
        textPercent = findViewById(R.id.textPercent);

        seekBar = findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                textPercent.setText(String.valueOf(seekBar.getProgress()) + "%");
                textValorTaxa.setText(String.valueOf(calcularTaxa()));
                textValorFinal.setText(String.valueOf(calcularFinal()));

            }
        });

    }

    protected Double calcularTaxa(){
        Double valorIni = Double.valueOf(editValorIni.getText().toString());
        Double taxa = Double.valueOf(seekBar.getProgress());
        return valorIni*(taxa/100);
    }

    protected Double calcularFinal(){
        Double valorIni = Double.valueOf(editValorIni.getText().toString());
        Double taxa = calcularTaxa();
        return valorIni + taxa;
    }

}
